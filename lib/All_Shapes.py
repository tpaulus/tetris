#! /usr/bin/python
#Written By Tom Paulus, @tompaulus, www.tompaulus.com

from graphics import *
from Shape import Shape


class I_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 2, center.y),
                  Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, color_rgb(141, 1, 228))  # Dark Blue
        self.shift_rotation_dir = True
        self.center_block = self.blocks[2]


class J_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y),
                  Point(center.x + 1, center.y + 1)]
        Shape.__init__(self, coords, color_rgb(255, 156, 5))  # Orange
        self.center_block = self.blocks[1]


class L_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y),
                  Point(center.x - 1, center.y + 1)]
        Shape.__init__(self, coords, color_rgb(58, 168, 242))  # Light Blue
        self.center_block = self.blocks[1]


class O_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x, center.y),
                  Point(center.x - 1, center.y),
                  Point(center.x, center.y + 1),
                  Point(center.x - 1, center.y + 1)]
        Shape.__init__(self, coords, color_rgb(232, 12, 16))  # Red
        self.center_block = self.blocks[0]

    def rotate(self, board):
        # Override Shape's rotate method since O_Shape does not rotate
        return


class S_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x, center.y),
                  Point(center.x, center.y + 1),
                  Point(center.x + 1, center.y),
                  Point(center.x - 1, center.y + 1)]
        Shape.__init__(self, coords, color_rgb(1, 170, 96))  # Green
        self.center_block = self.blocks[0]
        self.shift_rotation_dir = True
        self.rotation_dir = -1


class T_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y),
                  Point(center.x, center.y + 1)]
        Shape.__init__(self, coords, color_rgb(255, 245, 7))  # Yellow
        self.center_block = self.blocks[1]


class Z_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x, center.y + 1),
                  Point(center.x + 1, center.y + 1)]
        Shape.__init__(self, coords, color_rgb(251, 38, 147))  # Pink
        self.center_block = self.blocks[1]
        self.shift_rotation_dir = True
        self.rotation_dir = -1
