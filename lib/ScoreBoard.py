#! /usr/bin/python
#Written By Tom Paulus, @tompaulus, www.tompaulus.com

from graphics import *


class ScoreBoard():
    def __init__(self, win, width, height, score):
        self.width = width
        self.height = height
        self.score = score
        self.canvas = CanvasFrame(win, self.width, self.height)
        self.canvas.setBackground('white')
        self.score_message = Text(Point(150, 25), "Score: " + str(self.score))
        self.score_message.setSize(20)
        self.score_message.setTextColor("black")
        self.score_message.draw(self.canvas)

    def set_score(self, score):
        self.score_message.setText("Score: " + str(int(score)))

    def set_final_score(self, score):
        self.score_message.setText("Your Final Score: " + str(int(score)))

    def pause_score(self, score):
        self.score_message.setText("~~ PAUSED ~~   Score: " + str(int(score)))